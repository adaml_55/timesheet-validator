FROM openjdk:8-jre-alpine


WORKDIR /app

# Add app
ADD target/timesheet-validator-0.0.1-SNAPSHOT.jar /app


EXPOSE 4567
CMD ["/usr/bin/java", "-jar", "timesheet-validator-0.0.1-SNAPSHOT.jar"]
