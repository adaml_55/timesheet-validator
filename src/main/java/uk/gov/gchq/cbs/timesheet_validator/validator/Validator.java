package uk.gov.gchq.cbs.timesheet_validator.validator;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONObject;
import org.json.JSONTokener;

import spark.Request;
import uk.gov.gchq.cbs.timesheet_validator.Props;

public class Validator {

	// The control characters that are not allowed. TAB and CR are OK
	private static final Pattern pattern = Pattern.compile("[\\x00-\\x08\\x0B-\\x0C\\x0E-\\x1F\\x7F-\\xFF]");

	private static Logger logger = Logger.getLogger(Validator.class.getName());

	private static String PERMITTED_CONTENT_TYPE = "application/json;charset=utf-8";

	private static Schema schema = null;
	

	// Check string is valid ascii
	private static CharsetEncoder asciiEncoder = Charset.forName("ISO-8859-1").newEncoder();

	public Validator() {
	}

	public ValidationOutcome validateHeader(final Request request) throws Exception {

		ValidationOutcome validationOutcome = new ValidationOutcome();

		String contentTypeNoSpaces = request.contentType().replaceAll("\\s+","");		
		if (!contentTypeNoSpaces.equalsIgnoreCase(PERMITTED_CONTENT_TYPE)) {
			String msg = "Request must have a content-type of " + PERMITTED_CONTENT_TYPE;
			logger.info(msg);
			validationOutcome.addError(msg);
		}
		
		return validationOutcome;
	}

	public ValidationOutcome validateJson(final String json) throws Exception {

		ValidationOutcome validationOutcome = new ValidationOutcome();

		validateControlChars(json, validationOutcome);
		validateSchema(json, validationOutcome);

		return validationOutcome;
	}

	private void validateControlChars(final String str, final ValidationOutcome validationOutcome) {

		// Ensure only ASCII chars are present
		if (!isAscii(str)) {
			String msg = "JSON contains non ASCII characters";
			logger.info(msg);
			validationOutcome.addError(msg);
		}

		// Ensure only printable chars are present
		if (pattern.matcher(str).find()) {
			String msg = "JSON contains control characters";
			logger.info(msg);
			validationOutcome.addError(msg);
		}
	};

	private void validateSchema(final String json, final ValidationOutcome validationOutcome) throws Exception {

		if (null == schema) {
				loadSchema();
		}
	
		try {
			JSONObject jsonSubject = new JSONObject(json);
			Validator.schema.validate(jsonSubject);
		} catch (ValidationException e) {
			StringBuilder sb = new StringBuilder("invalid JSON: ");
			sb.append(getValidationException(e, 1, validationOutcome));
			logger.info(sb.toString());
		} catch (Exception e) {
			String msg = "invalid JSON: " + e.getLocalizedMessage();
			validationOutcome.addError(msg);
			logger.info(msg);
		}
	}

	private String getValidationExceptions(final ValidationException ve, final int numTabs,
			final ValidationOutcome validationOutcome) {

		StringBuilder sb = new StringBuilder(ve.getLocalizedMessage());
		for (final ValidationException e : ve.getCausingExceptions()) {
			sb.append(getValidationException(e, numTabs + 1, validationOutcome));
		}
		return sb.toString();
	}

	private String getValidationException(final ValidationException ve, final int numTabs,
			final ValidationOutcome validationOutcome) {

		StringBuilder sb = new StringBuilder("\n");
		for (int tabCount = 0; tabCount < numTabs; tabCount++)
			sb.append("  ");
		sb.append(ve.getLocalizedMessage());
		sb.append(getValidationExceptions(ve, numTabs + 1, validationOutcome));
		validationOutcome.addError(ve.getLocalizedMessage());

		return sb.toString();
	}


	private static synchronized void loadSchema() throws Exception {

		if (null == schema) {
			Props props = new Props();
			String schemaDef = props.getProperty("timesheet.json.schema");
			if (null == schemaDef || schemaDef.isEmpty()) {
				String msg = "JSON schema not found";
				logger.severe(msg);
				throw new Exception(msg);
			}

			try {
				JSONObject jsonSchema = new JSONObject(new JSONTokener(schemaDef));
				schema = SchemaLoader.load(jsonSchema);
			} catch (Exception e) {
				String msg = "failed to load JSON schema: " + e.getLocalizedMessage();
				logger.severe(msg);
				throw new Exception(msg, e);
			} 
		}
	}

	private boolean isAscii(String v) {
		return asciiEncoder.canEncode(v);
	}
}
