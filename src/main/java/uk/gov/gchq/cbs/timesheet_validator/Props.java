package uk.gov.gchq.cbs.timesheet_validator;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.Properties;
import java.util.logging.Logger;

public class Props {

	private static Logger logger = Logger.getLogger(Props.class.getName());

	private static final String PROPS_FILE = "application.properties";

	private Properties properties = null;

	public Props() {
	}

	public String getProperty(final String name) {

		if (null == properties) {
			initProperties();
		}
		return properties.getProperty(name);
	}

	private void initProperties() {
		this.properties = new Properties();

		try {
			InputStream ins = Props.class.getClassLoader().getResourceAsStream(PROPS_FILE);
			if (null != ins) {
				Reader reader = new InputStreamReader(ins, Charset.forName("UTF-8"));
				this.properties.load(reader);
			} else {
				String msg = "cannit find properties file:" + PROPS_FILE;
				logger.severe(msg);
				throw new IOException(msg);
			}
		} catch (IOException e) {
			String msg = "failed to read properties file:" + PROPS_FILE + ", error:" + e.getLocalizedMessage();
			logger.severe(msg);
		}
	}
}
