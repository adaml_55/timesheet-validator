package uk.gov.gchq.cbs.timesheet_validator.validator;

import java.util.ArrayList;
import java.util.List;

public class ValidationOutcome {

	private List<String> errors  = new ArrayList<>();
	
	
	public ValidationOutcome() {	
	}

	public ValidationOutcome(final List<String> errors) {
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}


	public void setErrors(final List<String> errors) {
		this.errors = errors;
	}

	public void addError(final String error) {
		this.errors.add(error);
	}

	public boolean isValid() {
		return 0 == this.errors.size();
	}
	
	public String getErrorsAsString() {
		StringBuilder sb = new StringBuilder();
		for (int count = 0; count < this.errors.size(); count++) {
			if (0 < count)
				sb.append(":");
			sb.append(this.errors.get(count));
		}
		return sb.toString();
	}

}
