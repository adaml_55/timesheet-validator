package uk.gov.gchq.cbs.timesheet_validator.response;

public class SuccessResponse {

	private String msg;

	public SuccessResponse() {
		super();
	}

	public SuccessResponse(final String msg) {
		super();
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(final String msg) {
		this.msg = msg;
	}

}
