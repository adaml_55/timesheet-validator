package uk.gov.gchq.cbs.timesheet_validator;

import static spark.Spark.post;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jetty.http.HttpStatus;

import com.google.gson.Gson;

import spark.Request;
import spark.Response;
import uk.gov.gchq.cbs.timesheet_validator.response.ErrorResponse;
import uk.gov.gchq.cbs.timesheet_validator.response.SuccessResponse;
import uk.gov.gchq.cbs.timesheet_validator.validator.ValidationOutcome;
import uk.gov.gchq.cbs.timesheet_validator.validator.Validator;

public class Application {

	private static Validator validator = new Validator();

	private static Gson gson = new Gson();

	public Application() {
		init();
	}

	public static void main(String[] args) {

		new Application();
	}

	private void init() {
		initPost();
	}

	private void initPost() {

		post("/jsonValidate", (request, response) -> {

			try {
				ValidationOutcome validationOutcome = validator.validateHeader(request);
				if (!validationOutcome.isValid()) {
					return gson.toJson(buildErrorResponse(request, response, validationOutcome.getErrors(),
							HttpStatus.BAD_REQUEST_400));
				}

				validationOutcome = validator.validateJson(request.body());
				if (!validationOutcome.isValid()) {
					return gson.toJson(buildErrorResponse(request, response, validationOutcome.getErrors(),
							HttpStatus.UNPROCESSABLE_ENTITY_422));

				}

				return gson.toJson(buildSuccessResponse(request, response));

			} catch (Exception e) {
				List<String> errors = new ArrayList<>();
				errors.add("internal server error:" + e.getLocalizedMessage());
				return gson.toJson(buildErrorResponse(request, response, errors, 
						 HttpStatus.INTERNAL_SERVER_ERROR_500));
			}
		});
	}

	private static ErrorResponse buildErrorResponse(final Request request, final Response response,
			final List<String> errors, final int httpStatus) {
		populateResponse(request, response, httpStatus);
		return new ErrorResponse(errors);
	}

	private static SuccessResponse buildSuccessResponse(final Request request, final Response response) {
		populateResponse(request, response, HttpStatus.OK_200);
		return new SuccessResponse("timesheet validated");
	}

	private static void populateResponse(final Request request, final Response response, final int httpStatus) {
		response.type("application/json");
		response.status(httpStatus);
		response.body(request.body());
	}
}
