package uk.gov.gchq.cbs.timesheet_validator.response;

import java.util.List;

public class ErrorResponse {

	private List<String> errors;

	public ErrorResponse() {
		super();
	}

	public ErrorResponse(final List<String> errors) {
		super();
		this.errors = errors;
	}

	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(final List<String> errors) {
		this.errors = errors;
	}

}
