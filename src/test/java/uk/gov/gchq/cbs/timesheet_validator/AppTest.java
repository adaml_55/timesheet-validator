package uk.gov.gchq.cbs.timesheet_validator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.util.stream.Collectors;

import org.eclipse.jetty.http.HttpStatus;
import org.junit.ClassRule;
import org.junit.Test;

import com.despegar.http.client.HttpClientException;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;

import spark.servlet.SparkApplication;

/**
 * Unit test for simple App.
 */
public class AppTest {

	public static class TestSparkApplication implements SparkApplication {
		@Override
		public void init() {
			new Application();
		}
	}

	@ClassRule
	public static SparkServer<TestSparkApplication> testServer = new SparkServer<>(AppTest.TestSparkApplication.class,
			4567);

	@Test
	public void testValidJson1() throws Exception {

		String body = readFile("test.json");
		String expectedResponse = "{\"msg\":\"timesheet validated\"}";
		doTest(body, HttpStatus.OK_200, expectedResponse);
	}

	@Test
	public void testValidJson2() throws Exception {

		String body = readFile("test2.json");
		String expectedResponse = "{\"msg\":\"timesheet validated\"}";
		doTest(body, HttpStatus.OK_200, expectedResponse);
	}

	@Test
	public void testInvalidJson1() throws Exception {

		String body = readFile("test1.json");
		String expectedResponse = "{\"errors\":[\"JSON contains non ASCII characters\",\"JSON contains control characters\"]}";
		doTest(body, HttpStatus.UNPROCESSABLE_ENTITY_422, expectedResponse);
	}

	@Test
	public void testInvalidJson2() throws Exception {

		String body = readFile("test3.json");
		String expectedResponse = "{\"errors\":[\"#/timesheet/times: expected minimum item count: 1, found: 0\"]}";
		doTest(body, HttpStatus.UNPROCESSABLE_ENTITY_422, expectedResponse);
	}

	@Test
	public void testInvalidJson3() throws Exception {

		String body = readFile("test4.json");
		String expectedResponse = "{\"errors\":[\"#/timesheet/times/0/startTime: string [08:61] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$\",\"#/timesheet/times/1/startTime: string [08:A0] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$\",\"#/timesheet/times: 2 schema violations found\"]}";
		doTest(body, HttpStatus.UNPROCESSABLE_ENTITY_422, expectedResponse);
	}
	
	@Test
	public void testInvalidJson4() throws Exception {

		String body = readFile("test5.json");
		String expectedResponse = "{\"errors\":[\"#/timesheet/times/0/startTime: string [08:61] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$\",\"#/timesheet/times/0: required key [endTime] not found\",\"#/timesheet/times/0: 2 schema violations found\",\"#/timesheet/times/1/startTime: string [08:A0] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$\",\"#/timesheet/times/1: required key [date] not found\",\"#/timesheet/times/1: 2 schema violations found\",\"#/timesheet/times: 2 schema violations found\"]}";
		doTest(body, HttpStatus.UNPROCESSABLE_ENTITY_422, expectedResponse);
	}

	@Test
	public void testInvalidContextType() throws Exception {

		String body = readFile("test1.json");
		String expectedResponse = "{\"errors\":[\"Request must have a content-type of application/json;charset\\u003dutf-8\"]}";
		doTest("application/json", body, HttpStatus.BAD_REQUEST_400, expectedResponse);
		
	}

	
	private String readFile(final String fileName) {
		String fileContents = null;
		InputStream ins = AppTest.class.getClassLoader().getResourceAsStream(fileName);
		if (null != ins) {
			Reader reader = new InputStreamReader(ins, Charset.forName("UTF-8"));
			BufferedReader buffer = new BufferedReader(reader);
			fileContents = buffer.lines().collect(Collectors.joining("\n"));
		}

		return fileContents;
	}

	private void doTest(final String contentType,
						final String body, 						
						final int expectedHttpResponseCode, 
						final String expectedResponseMessage) throws HttpClientException {

		/* The second parameter indicates whether redirects must be followed or not */
		PostMethod post = testServer.post("/jsonValidate", body, false);
		post.addHeader("Content-Type", contentType);
		HttpResponse httpResponse = testServer.execute(post);
		String responseBody = new String(httpResponse.body());

		System.out.println("expectedResponseMessage: " + expectedResponseMessage);
		System.out.println("responseBody: " + responseBody);
		
		assertEquals(expectedHttpResponseCode, httpResponse.code());
		assertEquals(expectedResponseMessage, responseBody);
		assertNotNull(testServer.getApplication());
	}
	
	private void doTest(final String body, 
			            final int expectedHttpResponseCode, 
			            final String expectedResponseMessage) throws HttpClientException {

		doTest("application/json;charset=UTF-8",
			   body,
			   expectedHttpResponseCode,
			   expectedResponseMessage);
	}
	

	
}
