CBS timesheet-validator
-----------------------

This project is a timesheet data validator for IMPEX. 
It is intended to run in a Docker container and be called from IMPEX.
It validates timesheet data that is sent low->high.


Requires: Java 8 or higher.



Possible return values:

1. OK (200)
   If timesheet is valid.
   The returned JSON = '{"msg":"timesheet validated"}'

2. UNPROCESSABLE_ENTIRY (422) 
   If timesheet data is invalid.
   The returned JSON = '{"errors":[<list of errors>]}'

   e.g.: 
   "errors":["#/timesheet/times/0/startTime: string [08:61] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$","#/timesheet/times/1/startTime: string [08:A0] does not match pattern ^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$","#/timesheet/times: 2 schema violations found"]}


3. BAD_REQUEST (400) 
   If non-json data is passed.
   The returned JSON = '{"errors":[<list of errors>]}'

   e.g.: 
   {"errors":["Request must have a content-type of application/json"]}



Building
--------
1. Build app

   "mvn clean install"


2. Build the Docker image

   docker build .



Running
-------
java -jar ./target/timesheet-validator-0.0.1-SNAPSHOT.jar

or 

docker run -p 4567:4567 <image>

